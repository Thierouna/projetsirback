package dao;

import entities.Ticket;
import jpa.EntityManagerHelper;

import java.util.List;

public class TicketDao extends  AbstractJpaDao<Long, Ticket> {

    public TicketDao() {
        this.setClazz(Ticket.class);
    }

    public List<Ticket> findTicketComment(){
        return EntityManagerHelper.getEntityManager().createQuery("select  t.messageList from Ticket t join t.messageList c "
                +"where  t.ticketId = c.id", Ticket.class).getResultList();
    }
}
