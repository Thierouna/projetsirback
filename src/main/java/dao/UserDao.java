package dao;

import entities.User;

public class UserDao extends AbstractJpaDao<Long, User> {

    public UserDao() {
        this.setClazz(User.class);
    }
}
