package entities;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@PrimaryKeyJoinColumn()
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue("FRF")
public class FeatureRequestFile extends  Ticket{
    private TypeFile typeFile;
    @Enumerated(EnumType.STRING)
    public TypeFile getTypeFile() {
        return typeFile;
    }

    public void setTypeFile(TypeFile typeFile) {
        this.typeFile = typeFile;
    }
}
