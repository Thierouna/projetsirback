package entities;

import com.fasterxml.jackson.annotation.*;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;
@NoArgsConstructor

@Entity
@Table(name = "USERS", uniqueConstraints = {
        @UniqueConstraint(columnNames = "user_name"),
        @UniqueConstraint(columnNames = "email")
        } )
public class User  implements Serializable {
    private Long userId;
    private String username;
    private  String firstname;
    private String lastname;
    private  String email;
    private  String passcode;
    private  List<Comment>commentList;
    private List<Ticket> ticketList;

    public User(String username, String passcode) {
        this.username = username;
        this.passcode = passcode;
    }

    @Id
@GeneratedValue
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
@Column(name="user_name")
@NotBlank
@Size(max = 50)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "user_firstname")
    @NotBlank
    @Size(max=50)
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String userName) {
        this.firstname = userName;
    }
    @Column(name = "user_lastname")
    @NotBlank
    @Size(max=50)
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Column(name = "email")
    @NotBlank
    @Size(max=100)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
@Column(name = "password")
@NotBlank
@Size(min=4, max=8)
    public String getPasscode() {
        return passcode;
    }

    public void setPasscode(String password) {
        this.passcode = password;
    }
@OneToMany(mappedBy = "user", fetch =FetchType.LAZY ,cascade = CascadeType.ALL, orphanRemoval = true)
@JsonIgnoreProperties("user")
    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties("user")
    public List<Ticket> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<Ticket> ticketList) {
        this.ticketList = ticketList;
    }
}
