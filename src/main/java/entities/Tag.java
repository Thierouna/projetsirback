package entities;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
@Entity
@Table(name = "TAGS", uniqueConstraints ={
        @UniqueConstraint(columnNames = "tagName")
})
@NoArgsConstructor
@AllArgsConstructor
public class Tag implements Serializable {
    private Long tagId;
    private String tagName;
    private List<Ticket> ticketList;

    @Id
@GeneratedValue
    public Long getTagId() {
        return tagId ;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tag) {
        this.tagName = tag;
    }

    @ManyToMany(mappedBy = "tagList", fetch = FetchType.EAGER)
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    @JsonIgnoreProperties("ticketList")
    public List<Ticket> getTicketList() {
        return ticketList;
    }

    public void setTicketList(List<Ticket> ticketList) {
        this.ticketList = ticketList;
    }
}
