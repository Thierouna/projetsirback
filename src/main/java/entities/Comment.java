package entities;
import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name = "COMMENTS")
@NoArgsConstructor
@AllArgsConstructor
public class Comment implements Serializable {
    private Long commentId;
    private String comment;
    private  User user;
    private Ticket ticket;

    @Id
@GeneratedValue
    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long id) {
        this.commentId = id;
    }

    public String getComment() {
        return comment;
    }
    @ManyToOne( fetch = FetchType.EAGER, cascade =
            CascadeType.MERGE)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setComment(String msg) {
        this.comment = msg;
    }
@ManyToOne(fetch = FetchType.EAGER)
@JoinColumn(name = "ticket_id", nullable = false)
@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
}
