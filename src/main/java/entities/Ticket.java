package entities;
import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
@Entity
@Table(name = "TICKETS")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "Type", length = 4)
@NoArgsConstructor
@AllArgsConstructor
public class Ticket implements Serializable {
    private Long ticketId;
    private String title;
    private String post;
    private User user;
    private List<Comment> messageList;
    private List<Tag> tagList;

    @Id
@GeneratedValue
@Column()
    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String titre) {
        this.title = titre;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String comment) {
        this.post = comment;
    }
@OneToMany(mappedBy = "ticket" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
@JsonBackReference
public List<Comment> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<Comment> messageList) {
        this.messageList = messageList;
    }
@ManyToOne(fetch = FetchType.EAGER)
@JoinColumn(name= "user_id",  referencedColumnName = "userId",nullable = false)
@JsonIgnoreProperties("ticketList")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToMany(fetch = FetchType.EAGER, cascade =
            CascadeType.PERSIST)
    @JoinTable(name = "TICKETS_TAGS",
            joinColumns = @JoinColumn(name = "ticket_id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "tag_id", nullable = false))
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    @JsonIgnoreProperties("tagList")
    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }
}
