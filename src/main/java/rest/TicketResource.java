package rest;


import dao.TicketDao;
import dto.TicketDto;
import entities.BugFile;
import entities.FeatureRequestFile;
import entities.Ticket;
import entities.TypeFile;
import io.swagger.v3.oas.annotations.Parameter;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/api/ticket")
@Produces({MediaType.APPLICATION_JSON})
public class TicketResource {
    private TicketDao td;

    public TicketResource() {
        this.td = new TicketDao();
    }

    @GET
    @Path("/{id}")
    public TicketDto getTicketById(@PathParam("id") Long id){
        Ticket t = td.findOne(id);
        TicketDto tdo = new TicketDto();
        tdo.setTitle(t.getTitle());
        tdo.setPost(t.getPost());
        return tdo;
    }
    @GET
    @Path("/all")
    public List<Ticket> getAllTickets(){
        return td
                .findAll();
    }
    @POST
    @Path("/bug/file")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addBugTicket(@Parameter(description = "Ticket object that needs to be added to the store",
            required = true) BugFile bugFile){
        BugFile ticket1 = new BugFile();
         ticket1.setTitle(bugFile.getTitle());
         ticket1.setPost(bugFile.getPost());
         ticket1.setUser(bugFile.getUser());
         ticket1.setTypeFile(TypeFile.Bugs);
        td.save(ticket1);
        return Response.ok(ticket1).build();
    }
    @POST
    @Path("/feature/request/file")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addFeatureRequestTicket(@Parameter(description = "Ticket object that needs to be added to the store",
            required = true) BugFile bugFile){
        FeatureRequestFile ticket1 = new FeatureRequestFile();
        ticket1.setTitle(bugFile.getTitle());
        ticket1.setPost(bugFile.getPost());
        ticket1.setUser(bugFile.getUser());
        ticket1.setTypeFile(TypeFile.FeaturesRequest);
        td.save(ticket1);
        return Response.ok(ticket1).build();
    }
    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response upgradeTicket(@Parameter(description = "Ticket object that needs to be added in the store",
            required = true) Ticket ticket, @PathParam("id") Long id){
      Ticket t = td.findOne(id);
      t.setTitle(ticket.getTitle());
      t.setPost(ticket.getPost());
      td.update(t);
        return  Response.ok(t).build();
    }
    @DELETE
    @Path("/{id}")
    public boolean deleteTicket(@PathParam("id")Long id){
        Ticket t = td.findOne(id);
        td.delete(t);
        return true;
    }
    @GET
    @Path("/comment")
    public  List<Ticket> commentOfTicket(){
        return  td.findTicketComment();
    }
}
