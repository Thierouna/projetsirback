package rest;

import dao.TagDao;
import dto.TagDto;
import entities.Tag;
import io.swagger.v3.oas.annotations.Parameter;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/api/tag")
@Produces({MediaType.APPLICATION_JSON})
public class TagResource {
    private TagDao td;

    public TagResource() {
        this.td = new TagDao();
    }

    @GET
    @Path("/{id}")
    public TagDto getTagById(@PathParam("id")Long id){
        Tag t = td.findOne(id);
        TagDto tdo = new TagDto();
        tdo.setTagName(t.getTagName());
        tdo.setTicketList(t.getTicketList());
        return tdo;
    }
    @GET
    @Path("/all")
    public List<Tag> getAllTags(){
        return td.
                findAll();
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addTag(@Parameter(description = "Tag object that needs to be added to the store",
            required = true)Tag tag){
       td.save(tag);
        return  Response.ok(tag).build();
    }
    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response upgradeTag(@Parameter(description = "Tag object that needs to be added in the store",
            required = true) Tag tag, @PathParam("id")Long id){
        Tag _tag = td.findOne(id);
       _tag.setTagName(tag.getTagName());
        td.update(_tag);
        return  Response.ok(tag).build();
    }
    @DELETE
    @Path("/{id}")
    public Response deleteTag(@PathParam("id")Long id){
        Tag tag = td.findOne(id);
        td.delete(tag);
        return  Response.ok().entity(tag).build();
    }

}
