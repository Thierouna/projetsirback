package rest;
import dao.UserDao;
import dto.UserDto;
import responses.MessageResponse;
import entities.User;
import io.swagger.v3.oas.annotations.Parameter;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;
@Path("/api/user")
@Produces({MediaType.APPLICATION_JSON})
public class UserResource {
    private static final Logger logger = Logger.getLogger(UserResource.class.getName());

    private UserDao ud;

    public UserResource() {
        this.ud = new UserDao();
    }

    @GET
    @Path("/{id}")
    public UserDto getUserById(@PathParam("id") Long id){
           User u =   ud.findOne(id);
           UserDto _u = new UserDto();
           _u.setFirstname(u.getFirstname());
           _u.setLastname(u.getLastname());
           _u.setTicketList(u.getTicketList());
           _u.setCommentList(u.getCommentList());
           return  _u;
    }
    @GET
    @Path("/all")
    public List<User> getAllUsers(){
        return ud
                .findAll();
    }
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response authenticateUser(UserDto dto) {
        return  Response.ok(new User(dto.getUsername(),
                dto.getPasscode())).build();
    }
    @POST
    @Path("/signup")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerUser(User user){
            ud.save(user);
            return Response.ok(new MessageResponse("User registered successfully")).build();
    }
    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response upgradeUser(@Parameter(description = "User object that needs to be updated in the store",
            required = true) User user, @PathParam("id") Long id){
        User u = ud.findOne(id);
       ud.update(u);
        return Response.ok().entity(u).build();
    }
    @DELETE()
    @Path("/{id}")
    public Response deleteUser(@PathParam("id")Long id){
        User u = ud.findOne(id);
        ud.delete(u);
        return Response.ok(u).build();
    }
}
