package rest;

import dao.CommentDao;
import dto.CommentDto;
import entities.Comment;
import io.swagger.v3.oas.annotations.Parameter;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/api/comment")
@Produces({MediaType.APPLICATION_JSON})

public class CommentResource {
    private CommentDao cd;

    public CommentResource() {
        this.cd = new CommentDao();
    }

    @GET
    @Path("/{id}")
    public CommentDto getMessageById(@PathParam("id")Long id){
        Comment m  = cd.findOne(id);
        CommentDto md = new CommentDto();
        md.setComment(m.getComment());
        md.setUser(m.getUser());
        md.setTicket(m.getTicket());
        return md;
    }
    @GET
    @Path("/all")
    public List<Comment> getAllComments(){
        return cd.
                findAll();
    }
    @POST()
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addComment(@Parameter(description = "Message object that needs to be added to the store"
            , required = true) Comment message){
        cd.save(message);
        return Response.ok(message).build();
    }
    @PUT()
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response upgradeComment(@Parameter(description = "Message object that needs to be added in the store",
            required = true) Comment message, @PathParam("id")Long id){
        Comment _comment = cd.findOne(id);
       _comment.setComment(message.getComment());
       cd.update(_comment);
        return  Response.ok(message).build();
    }
    @DELETE()
    @Path("/{id}")
    public Response deleteComment(@PathParam("id")Long id){
        Comment c = cd.findOne(id);
        cd.delete(c);
        return  Response.ok().entity(c).build();
    }
}
