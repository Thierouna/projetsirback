package dto;


import entities.TypeFile;

public class TicketDto {
private String title;
private String post;
private Long userId;
private TypeFile typeFile;
    public TicketDto() {
        super();
    }

    public TicketDto(String title, String post, Long userId) {
        this.title = title;
        this.post = post;
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public TypeFile getTypeFile() {
        return typeFile;
    }

    public void setTypeFile(TypeFile typeFile) {
        this.typeFile = typeFile;
    }
}
