package responses;

public class JwtResponse {
    private  Long _id;
    private String token, username, email;
    public JwtResponse(String token, Long _id, String username, String email){
        this.token = token;
        this._id = _id;
        this.username = username;
        this.email = email;
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
