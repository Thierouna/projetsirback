package responses;

public class MessageResponse {
    private String comment;

    public MessageResponse(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
